# all the imports
import sqlite3
from flask import Flask, request, session, g, redirect, url_for, \
	abort, render_template, flash

from contextlib import closing

# configuration
DATABASE = '/tmp/flaskr.db'
DEBUG = True
SECRET_KEY = 'development key'
USERNAME = 'admin'
PASSWORD = 'default'

# create our little application :)
app = Flask(__name__)
# from_object() 将会寻找给定的对象(如果它是一个字符串，则会导入它)， 
# 搜寻里面定义的全部大写的变量。在我们的这种情况中，配置文件就是我们上面写的几行代码。 
# 你也可以将他们分别存储到多个文件。
app.config.from_object(__name__)
# app.config.from_envvar('FLASK_SETTINGS', silent=True)

def connect_db():
	return sqlite3.connect(app.config['DATABASE'])

# closing() 函数允许我们在 with 块中保持数据库连接可用。 
# 应用对象的 open_resource() 方法在其方框外也支持这个功能， 因此可以在 with 块中直接使用。
# 这个函数从资源位置（你的 flaskr 文 件夹）中打开一个文件，并且允许你读取它。
# 我们在这里用它在数据库连接上执行一个脚本。
def init_db():
	with closing(connect_db()) as db:
		with app.open_resource('schema.sql') as f:
			db.cursor().executescript(f.read().decode())
		db.commit()

# 使用 before_request() 装饰器的函数会在请求之前被调用而且不带参数
@app.before_request
def before_request():
    g.db = connect_db()

# 使用 teardown_request() 装饰器的函数将在响应构造后执行， 并不允许修改请求，返回的值会被忽略。
# 如果在请求已经被处理的时候抛出异常，它会被传递到每个函数， 否则会传入一个 None 。
@app.teardown_request
def teardown_request(exception):
    g.db.close()

# 显示条目
@app.route('/')
def show_entries():
    cur = g.db.execute('select id, title, text from entries order by id desc')
    entries = [dict(id=row[0], title=row[1], text=row[2]) for row in cur.fetchall()]
    return render_template('show_entries.html', entries=entries)

# 添加新条目
@app.route('/add', methods=['POST'])
def add_entry():
    if not session.get('logged_in'):
        abort(401)
    g.db.execute('insert into entries (title, text) values (?, ?)',
                 [request.form['title'], request.form['text']])
    g.db.commit()
    flash('New entry was successfully posted')
    return redirect(url_for('show_entries'))

@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        if request.form['username'] != app.config['USERNAME']:
            error = 'Invalid username'
        elif request.form['password'] != app.config['PASSWORD']:
            error = 'Invalid password'
        else:
            session['logged_in'] = True
            flash('You were logged in')
            return redirect(url_for('show_entries'))
    return render_template('login.html', error=error)

@app.route('/logout')
def logout():
    # 如果你使用字典的 pop() 方法并传入第二个参数（默认）， 
    # 这个方法会从字典中删除这个键，如果这个键不存在则什么都不做。
    # 这很有用，因为 我们不需要检查用户是否已经登入。
    session.pop('logged_in', None)
    flash('You were logged out')
    return redirect(url_for('show_entries'))
    
if __name__ == '__main__':
	app.run()















































